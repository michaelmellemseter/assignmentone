import java.util.Scanner;

class Rectangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  // Creating a Scanner object
        boolean finished = false;
        int width = 0;
        int height = 0;
        char character = '#';

        while (!finished) {  //Checking if the user wants more rectangles printed
            try{
                System.out.println("Enter the width of the rectangle (needs to be an integer), type anything else to be finished");
                width = scanner.nextInt();
            } catch (Exception e) {  //If the user puts anything else but an integer the program will stop
                System.out.println("You are finished");
                finished = true;
            }

            if (!finished) {
                try{
                    System.out.println("Enter the width of the rectangle (needs to be an integer), type anything else to be finished");
                    height = scanner.nextInt();
                } catch (Exception e) {  //If the user puts anything else but an integer the program will stop
                    System.out.println("You are finished");
                    finished = true;
                }
            }

            if (!finished) {
                if (width < 1 || height < 1) {  //Checking if the userinput is above 0
                    System.out.println("Both numbers needs to be above 0");
                    finished = true;
                }
            }

            if (!finished) {
                try{
                    System.out.println("Type the character you want your rectangle to be, if you write multiple characters only the first character will be used");
                    character = scanner.next().charAt(0); 
                } catch (Exception e) {  //If the user puts anything that will give an exception the program will stop
                    System.out.println("You are finished");
                    finished = true;
                }
            }

            if (!finished) {
                printRectangle(width, height, character);
            }

            if (!finished && height > 6 && width > 6) {
                printDoubleRectangle(width, height, character);
            }
         

        }
        scanner.close();
    }

    private static void printRectangle(int width, int height, char character) {  //Method for printing a rectangle using #
        for (int i = 0; i < height; i++) {
            System.out.println();
            for(int j = 0; j < width; j++) {
                if (i == 0 || i == height-1 || j == 0 || j == width-1) {  //Checking if there should be printed a #
                    System.out.print(character);
                } else {
                    System.out.print(" ");
                }
            }
        }
        System.out.println();
    }

    private static void printDoubleRectangle(int width, int height, char character) {  //Method for printing two rectangles using #
        for (int i = 0; i < height; i++) {
            System.out.println();
            for(int j = 0; j < width; j++) {
                if (i == 0 || i == height-1 || j == 0 || j == width-1 ||
                i == 2 && j > 1 && j < width-2 || 
                i == height-3 && j > 1 && j < width-2 || 
                j == 2 && i > 1 && i < height-2 || 
                j == width-3 && i > 1 && i < height-2) {  //Checking if there should be printed a #
                    System.out.print(character);
                } else {
                    System.out.print(" ");
                }
            }
        }
        System.out.println();
    }
}